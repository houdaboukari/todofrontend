import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';
import { AddToDoComponent } from './components/add-to-do/add-to-do.component';
import { ListToDoComponent } from './components/list-to-do/list-to-do.component';



const routes: Route[] = [
  {path: '', redirectTo: '/list', pathMatch: 'full'},
  {path: 'add', component: AddToDoComponent},
  {path: 'list', component: ListToDoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
