import { Injectable } from '@angular/core';
import { Todo } from '../models/todo';
import {MatSnackBar} from '@angular/material/snack-bar';


@Injectable({
  providedIn: 'root'
})
export class ToDoService {

  //Déclaration TodoList d'objets todo : vide
  todoList: Todo[] = [];

  //Appeler bibliothèque MatsNackBar
  constructor(private _snackBar: MatSnackBar) { }

  //Service pour supprimer todo
  deleteTodo(item : Todo) {
    //obtenir l'index de l'élément
    let index = this.todoList.indexOf(item);
    //modifié le contenu de tableau
    this.todoList.splice(index, 1);
    //message lors de la suppression de todo
    this._snackBar.open("Todo succefully deleted","ok");
  }

  addTodo(title : string) {
    //ajouter un element avec id incrémenter par 1 chaque fois
    let id = this.todoList.length + 1;
    //préparation de l'objet a ajouter
    const item: Todo = {
      id: id,
      isCompleted: false,
      date: new Date(),
      title: title
    }
    //ajouter l'objet préparer au début de la liste
    this.todoList.unshift(item);
  }

}
