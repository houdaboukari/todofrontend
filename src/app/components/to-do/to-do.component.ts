import { Component, Input, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Todo } from 'src/app/models/todo';
import {ToDoService} from 'src/app/services/to-do.service';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.css']
})
export class ToDoComponent implements OnInit {

  //Partage de données entre les composants : list-to-do et to-do
  @Input() todoInput!: Todo;

  //Déclaration de boolean
  //Pour todo done ou pas
  completed: boolean = false;

  //Déclaration todo sous type Todo
  todo!: Todo;

  //Appeler le service todoService et une bibliothèque MatSnackBar
  constructor(public todoService: ToDoService, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  //Méthode pour modifier la statut de todo
  onChange() {
    console.log("completed : changed");

    //Inverser le variable
    this.completed = !this.completed;

    //Affichage de message si todo est complete
    if(this.completed){
      this._snackBar.open("Todo succefully completed","ok",{
        duration: 3000
      });
    }
  }



  //delete todo from list
  deleteTodo(item : Todo) {
    this.todo = item;
    this.todoService.deleteTodo(item);
    this._snackBar.open("Todo succefully deleted","ok",{
      duration: 3000
    });
    console.log("todo deleted !");

  }

}
