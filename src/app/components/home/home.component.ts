import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddToDoComponent } from '../add-to-do/add-to-do.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }
 // methode pour ouvrir une fenetre de dialog contient le composant "AddToDoComponent"
  openDialog(): void {
    const dialogRef = this.dialog.open(AddToDoComponent, {
      // Width de la fenetre de 400px
      width: '400px',

    });
    //Action lorsque le dialogue est fermé
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}

