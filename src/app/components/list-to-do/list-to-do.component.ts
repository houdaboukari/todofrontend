import { Component, OnInit } from '@angular/core';
import {ToDoService} from 'src/app/services/to-do.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-to-do',
  templateUrl: './list-to-do.component.html',
  styleUrls: ['./list-to-do.component.css']
})
export class ListToDoComponent implements OnInit {

  //Appeler le service todoService
  constructor(public todoService: ToDoService) { }

  ngOnInit(): void {

  }
}
