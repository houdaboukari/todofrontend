import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Todo } from 'src/app/models/todo';
import {ToDoService} from 'src/app/services/to-do.service';

@Component({
  selector: 'app-add-to-do',
  templateUrl: './add-to-do.component.html',
  styleUrls: ['./add-to-do.component.css']
})
export class AddToDoComponent implements OnInit {

  // Déclaration de variable string vide
  todo = '';

    /*
     Constructor for  class
    */
  constructor(public todoService: ToDoService,
    public dialogRef: MatDialogRef<AddToDoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Todo,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  // Méthode pour ajout de todo
  onSubmit(){
    // fait appel à la méthode addTodo de service todoService pour envoyer la requête de post
    this.todoService.addTodo(this.todo);
    this.todo = '';
    // snackbar pour informé que la requête est bien envoyé
    this._snackBar.open("Todo succefully added","ok",{
      duration: 3000
    });
    console.log("todo Added !");

  }
  //methode pour fermer dialog
  onNoClick(): void {
    this.dialogRef.close();
  }

}
